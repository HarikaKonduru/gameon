package com.snipe.gameon;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.snipe.gameon.config.GameOnApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=GameOnApplication.class)
public class GameOnApplicationTests {

	@Test
	public void contextLoads() {
	}

}
