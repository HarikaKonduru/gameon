DROP DATABASE IF EXISTS `gameon`;
CREATE DATABASE gameon;


USE `gameon`;


DROP TABLE IF EXISTS `Participant`;

CREATE TABLE `Participant` (
  `participantId` varchar(64) ,
  `userName` varchar(128) DEFAULT NULL UNIQUE,
  `firstName` varchar(64) DEFAULT NULL,
  `lastName` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `gender` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `phoneNumber` varchar(64) DEFAULT NULL,
  `notification` varchar(64) DEFAULT NULL,
  `events` varchar(128) DEFAULT NULL,
  `rating` varchar(64) DEFAULT NULL,
  `favouriteEventers` varchar(512)  DEFAULT NULL,
  `interests` varchar(128)  DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT 1,
  `createdDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`participantId`)
) ;

ALTER TABLE Participant ADD INDEX  Participant_participantId (participantId);




DROP TABLE IF EXISTS `BusinessHost`;

CREATE TABLE `BusinessHost` (
  `bhId` varchar(64) ,
  `type` varchar(64) DEFAULT NULL,
  `userName` varchar(128) DEFAULT NULL UNIQUE,
  `companyName` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `businessPhone` varchar(64) DEFAULT NULL,
   `cellPhone` varchar(64) DEFAULT NULL,
  `notification` varchar(64) DEFAULT NULL,
  `eventType` varchar(128) DEFAULT NULL,
  `address` varchar(64) DEFAULT NULL,
  `cantactPerson` varchar(512)  DEFAULT NULL,
  `venue` varchar(128)  DEFAULT NULL,
  `adharNumber` varchar(64) DEFAULT NULL UNIQUE,
  `isActive` tinyint(1) DEFAULT 1,
  `createdDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`bhId`)
) ;

ALTER TABLE BusinessHost ADD INDEX  businessHost_bhId (bhId);





DROP TABLE IF EXISTS `IndividualHost`;

CREATE TABLE `IndividualHost` (
  `ihId` varchar(64) ,
  `type` varchar(64) DEFAULT NULL,
  `userName` varchar(128) DEFAULT NULL UNIQUE,
  `firstName` varchar(64) DEFAULT NULL,
  `lastName` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `gender` varchar(64) DEFAULT NULL,
  `dob` date DEFAULT NULL,
   `phoneNumber` varchar(64) DEFAULT NULL,
   `mobileNumber` varchar(64) DEFAULT NULL,
  `notification` varchar(64) DEFAULT NULL,
  `eventType` varchar(128) DEFAULT NULL,
  `address` varchar(64) DEFAULT NULL,
  `rating` varchar(512)  DEFAULT NULL,
  `eventName` varchar(128)  DEFAULT NULL,
   `eventDescription` varchar(128)  DEFAULT NULL,
  `hostedEvents` varchar(64) DEFAULT NULL,
   `trackInterests` varchar(128)  DEFAULT NULL,
    `dateOfEvent` date  DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT 1,
  `createdDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ihId`)
) ;








DROP TABLE IF EXISTS `Documents`;


CREATE TABLE `Documents` (
  `fileName` varchar(64) ,
  `fileDownloadUri` varchar(64) DEFAULT NULL,
  `fileType` varchar(128) DEFAULT NULL ,
  `size` long DEFAULT NULL
  
) ;