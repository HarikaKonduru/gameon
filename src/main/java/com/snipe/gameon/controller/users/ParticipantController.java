package com.snipe.gameon.controller.users;



	import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.constants.StatusCode;
import com.snipe.gameon.model.users.ParticipantModel;
import com.snipe.gameon.response.ErrorObject;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.service.users.ParticipantService;
import com.snipe.gameon.utils.CommonUtils;

	

	@RestController
	@RequestMapping("/v1")
	public class ParticipantController implements Constants {

		private static final Logger logger = LoggerFactory.getLogger(ParticipantController.class);
		
		@Autowired
		ParticipantService participantService;
		
	

		@RequestMapping(value = "/participant", method = RequestMethod.POST, produces = "application/json")
		public Response saveParticipant(@RequestBody ParticipantModel participant, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
			logger.info("addParticipant: Received request URL: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("addParticipant: Received request: " + CommonUtils.getJson(participant));
			return participantService.saveParticpant(participant);
		}

		@RequestMapping(value = "/participant", method = RequestMethod.PUT, produces = "application/json")
		public Response updateParticipant(@RequestBody ParticipantModel participant, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
			logger.info("updateParticipant: Received request URL: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("updateParticipant: Received request: " + CommonUtils.getJson(participant));
			return participantService.updateParticipant(participant);
		}

		@RequestMapping(value = "/participant/{participantId}", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getUser(@PathVariable("participantId") String participantId, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getParticipant: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			ParticipantModel participantModel = participantService.getParticipant(participantId);
			Response res = CommonUtils.getResponseObject("Participant Details");
			if (participantModel == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Participants Not Found", "Participants Not Found");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(participantModel);
			}
			logger.info("getParticipant: Sent response");
			return CommonUtils.getJson(res);
		}

		@RequestMapping(value = "/participant/{participantId}", method = RequestMethod.DELETE, produces = "application/json")
		public @ResponseBody Response deleteUser(@PathVariable("participantId") String participantId, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getParticipant: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			return participantService.deleteParticipant(participantId);
		}

		@RequestMapping(value = "/participantExist/{participantName}", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String isParticipantNameExist(@PathVariable("participantName") String participantName, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getParticipant: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			boolean isParticipantNameExist = participantService.isParticipantNameExist(participantName);
			Response res = CommonUtils.getResponseObject("Participant Exist");
			Map<String, Boolean> obj = new HashMap<String, Boolean>();
			obj.put("isParticipantNameExist", isParticipantNameExist);
			res.setData(obj);
			if (!isParticipantNameExist) {
				res.setStatus(StatusCode.ERROR.name());
			}
			logger.info("getParticipant: Sent response");
			return CommonUtils.getJson(res);
		}

		@RequestMapping(value = "/participants", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getParticipants(HttpServletRequest request, HttpServletResponse response) throws Exception {
			logger.info("getParticipants: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			List<ParticipantModel> participants = participantService.getParticipants();
			Response res = CommonUtils.getResponseObject("List of Participants");
			if (participants == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Participants Not Found", "Participants Not Found");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(participants);
			}
			logger.info("getParticipants: Sent response");
			return CommonUtils.getJson(res);
		}
		
		@RequestMapping(value = "/plogin", method = RequestMethod.POST, produces = "application/json")
		public @ResponseBody String authenticateP(@RequestBody ParticipantModel participant, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("authenticate: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("authenticate :Received request: " + CommonUtils.getJson(participant));
			participant = participantService.authenticateP(participant);
			Response res = CommonUtils.getResponseObject("authenticate participant");
			if (participant == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Invalid Username or Password",
						"Invalid Username or Password");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(participant);
			}
			logger.info("authenticateP: Sent response");
			return CommonUtils.getJson(res);
		}
	



}
