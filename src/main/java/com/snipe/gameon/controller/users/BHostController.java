package com.snipe.gameon.controller.users;




	import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.constants.StatusCode;
import com.snipe.gameon.model.users.BHostModel;
import com.snipe.gameon.response.ErrorObject;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.service.users.BHostService;
import com.snipe.gameon.utils.CommonUtils;

	

	@RestController
	@RequestMapping("/v1")
	public class BHostController implements Constants {

		private static final Logger logger = LoggerFactory.getLogger(BHostController.class);
		
		@Autowired
		BHostService bhostService;
		
	

		@RequestMapping(value = "/bhost", method = RequestMethod.POST, produces = "application/json")
		public Response saveBhost(@RequestBody BHostModel bhost, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
			logger.info("addBHost: Received request URL: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("addBHost: Received request: " + CommonUtils.getJson(bhost));
			return bhostService.saveBhost(bhost);
		}

		@RequestMapping(value = "/bhost", method = RequestMethod.PUT, produces = "application/json")
		public Response updateBhost(@RequestBody BHostModel bhost, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
			logger.info("updateBHost: Received request URL: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("updateBHost: Received request: " + CommonUtils.getJson(bhost));
			return bhostService.updateBhost(bhost);
		}

		@RequestMapping(value = "/bhost/{bhostId}", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getBhost(@PathVariable("bhostId") String bhostId, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getBHost: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			BHostModel bhostModel = bhostService.getBhost(bhostId);
			Response res = CommonUtils.getResponseObject("Business Host Details");
			if (bhostModel == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Hosts Not Found", "Hosts Not Found");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(bhostModel);
			}
			logger.info("getBhost: Sent response");
			return CommonUtils.getJson(res);
		}

		@RequestMapping(value = "/bhost/{bhostId}", method = RequestMethod.DELETE, produces = "application/json")
		public @ResponseBody Response deleteBhost(@PathVariable("bhostId") String bhostId, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getBhost: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			return bhostService.deleteBhost(bhostId);
		}

		@RequestMapping(value = "/bhostExist/{bhostName}", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String isBhostNameExist(@PathVariable("bhostName") String bhostName, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getBhost: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			boolean isBhostNameExist = bhostService.isBhostNameExist(bhostName);
			Response res = CommonUtils.getResponseObject("Host Exist");
			Map<String, Boolean> obj = new HashMap<String, Boolean>();
			obj.put("isBhostNameExist", isBhostNameExist);
			res.setData(obj);
			if (!isBhostNameExist) {
				res.setStatus(StatusCode.ERROR.name());
			}
			logger.info("getBhost: Sent response");
			return CommonUtils.getJson(res);
		}

		@RequestMapping(value = "/bhosts", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getBhosts(HttpServletRequest request, HttpServletResponse response) throws Exception {
			logger.info("getBhosts: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			List<BHostModel> bhosts = bhostService.getBhosts();
			Response res = CommonUtils.getResponseObject("List of hosts");
			if (bhosts == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Hosts Not Found", "Hosts Not Found");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(bhosts);
			}
			logger.info("getBhosts: Sent response");
			return CommonUtils.getJson(res);
		}
		
		@RequestMapping(value = "/blogin", method = RequestMethod.POST, produces = "application/json")
		public @ResponseBody String authenticateBhost(@RequestBody BHostModel bhost, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("authenticate: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("authenticate :Received request: " + CommonUtils.getJson(bhost));
			bhost = bhostService.authenticateBhost(bhost);
			Response res = CommonUtils.getResponseObject("authenticate host");
			if (bhost == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Invalid Username or Password",
						"Invalid Username or Password");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(bhost);
			}
			logger.info("authenticate: Sent response");
			return CommonUtils.getJson(res);
		}
	





}
