package com.snipe.gameon.controller.users;
	




	import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.constants.StatusCode;
import com.snipe.gameon.model.users.IHostModel;
import com.snipe.gameon.response.ErrorObject;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.service.users.IHostService;
import com.snipe.gameon.utils.CommonUtils;

	

	@RestController
	@RequestMapping("/v1")
	public class IHostController implements Constants {

		private static final Logger logger = LoggerFactory.getLogger(IHostController.class);
		
		@Autowired
		IHostService ihostService;
		
	

		@RequestMapping(value = "/ihost", method = RequestMethod.POST, produces = "application/json")
		public Response saveIhost(@RequestBody IHostModel ihost, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
			logger.info("addIHost: Received request URL: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("addIhost: Received request: " + CommonUtils.getJson(ihost));
			return ihostService.saveIhost(ihost);
		}

		@RequestMapping(value = "/ihost", method = RequestMethod.PUT, produces = "application/json")
		public Response updateIhost(@RequestBody IHostModel ihost, HttpServletRequest request, HttpServletResponse response)
				throws Exception {
			logger.info("updateIHost: Received request URL: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("updateIhost: Received request: " + CommonUtils.getJson(ihost));
			return ihostService.updateIhost(ihost);
		}

		@RequestMapping(value = "/ihost/{ihostId}", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getIhost(@PathVariable("ihostId") String ihostId, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getIhost: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			IHostModel ihostModel = ihostService.getIhost(ihostId);
			Response res = CommonUtils.getResponseObject("Individual Host Details");
			if (ihostModel == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Hosts Not Found", "Hosts Not Found");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(ihostModel);
			}
			logger.info("getIhost: Sent response");
			return CommonUtils.getJson(res);
		}

		@RequestMapping(value = "/ihost/{ihostId}", method = RequestMethod.DELETE, produces = "application/json")
		public @ResponseBody Response deleteIhost(@PathVariable("ihostId") String ihostId, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getIhost: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			return ihostService.deleteIhost(ihostId);
		}

		@RequestMapping(value = "/ihostExist/{ihostName}", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String isIhostNameExist(@PathVariable("ihostName") String ihostName, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("getIhost: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			boolean isIhostNameExist = ihostService.isIhostNameExist(ihostName);
			Response res = CommonUtils.getResponseObject("Host Exist");
			Map<String, Boolean> obj = new HashMap<String, Boolean>();
			obj.put("isIhostNameExist", isIhostNameExist);
			res.setData(obj);
			if (!isIhostNameExist) {
				res.setStatus(StatusCode.ERROR.name());
			}
			logger.info("getIhost: Sent response");
			return CommonUtils.getJson(res);
		}

		@RequestMapping(value = "/ihosts", method = RequestMethod.GET, produces = "application/json")
		public @ResponseBody String getIhosts(HttpServletRequest request, HttpServletResponse response) throws Exception {
			logger.info("getIhosts: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			List<IHostModel> ihosts = ihostService.getIhosts();
			Response res = CommonUtils.getResponseObject("List of hosts");
			if (ihosts == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Hosts Not Found", "Hosts Not Found");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(ihosts);
			}
			logger.info("getIhosts: Sent response");
			return CommonUtils.getJson(res);
		}
		
		@RequestMapping(value = "/ilogin", method = RequestMethod.POST, produces = "application/json")
		public @ResponseBody String authenticateIhost(@RequestBody IHostModel ihost, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			logger.info("authenticate: Received request: " + request.getRequestURL().toString()
					+ ((request.getQueryString() == null) ? "" : "?" + request.getQueryString().toString()));
			logger.info("authenticate :Received request: " + CommonUtils.getJson(ihost));
			ihost = ihostService.authenticateIhost(ihost);
			Response res = CommonUtils.getResponseObject("authenticate host");
			if (ihost == null) {
				ErrorObject err = CommonUtils.getErrorResponse("Invalid Username or Password",
						"Invalid Username or Password");
				res.setErrors(err);
				res.setStatus(StatusCode.ERROR.name());
			} else {
				res.setData(ihost);
			}
			logger.info("authenticateIhost: Sent response");
			return CommonUtils.getJson(res);
		}
	







}
