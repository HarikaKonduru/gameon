package com.snipe.gameon.service.users;

import java.util.List;

import com.snipe.gameon.model.users.BHostModel;
import com.snipe.gameon.response.Response;

public interface BHostService {
	public Response saveBhost(BHostModel bhost) throws Exception;

	public Response updateBhost(BHostModel bhost) throws Exception;

	public Response deleteBhost(String bhId) throws Exception;

	public BHostModel getBhost(String bhId) throws Exception;

	public List<BHostModel> getBhosts() throws Exception;

	public boolean isBhostNameExist(String userName) throws Exception;

	public BHostModel authenticateBhost(BHostModel bhost) throws Exception;
	
}
