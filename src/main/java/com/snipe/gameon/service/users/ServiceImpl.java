package com.snipe.gameon.service.users;


	import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.snipe.gameon.config.GameOnProperties;
import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.dao.users.BHostDAO;
import com.snipe.gameon.dao.users.IHostDAO;
import com.snipe.gameon.dao.users.ParticipantDAO;
import com.snipe.gameon.domain.users.BusinessHost;
import com.snipe.gameon.domain.users.IndividualHost;
import com.snipe.gameon.domain.users.Participant;
import com.snipe.gameon.mapper.users.BHostMapper;
import com.snipe.gameon.mapper.users.IHostMapper;
import com.snipe.gameon.mapper.users.ParticipantMapper;
import com.snipe.gameon.model.users.BHostModel;
import com.snipe.gameon.model.users.IHostModel;
import com.snipe.gameon.model.users.ParticipantModel;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.utils.CommonUtils;

	@Service
	public class ServiceImpl implements ParticipantService,IHostService,BHostService, Constants {

		@Autowired
		ParticipantDAO participantDAO;

		@Autowired
		ParticipantMapper participantMapper;
		
		
		@Autowired
		BHostDAO bhostDAO;
		
		@Autowired
		BHostMapper bhostMapper;
		
		
		
		@Autowired
		IHostDAO ihostDAO;
		
		@Autowired
		IHostMapper ihostMapper;


		@Autowired
		GameOnProperties gameonProperties;

		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		private static final Logger logger = LoggerFactory.getLogger(ServiceImpl.class);

		public ServiceImpl() {
			// TODO
		}

		public Response saveParticpant(ParticipantModel participantModel) {
			try {
				Participant participant = new Participant();
				BeanUtils.copyProperties(participantModel, participant);
				participant.setParticipantId(CommonUtils.getRandomUUID());
				participant.setPassword(CommonUtils.encriptString(participant.getPassword()));
				participant.setIsActive(true);
				Response response = participantDAO.saveParticipant(participant);
				return response;
			} catch (Exception ex) {
				logger.info("Exception Service:" + ex.getMessage());
			}
			return null;
		}

		public ParticipantModel getParticipant(String participantId) {
			try {
				Participant participant = participantDAO.getParticipant(participantId);
				ParticipantModel participantModel = new ParticipantModel();
				if (participant == null)
					return null;
				BeanUtils.copyProperties(participant, participantModel);
				return participantModel;
			} catch (Exception e) {
				logger.info("Exception getParticipant:", e);
				return null;
			}
		}

		public Response deleteParticipant(String participantId) {
			try {
				return participantDAO.deleteParticipant(participantId);
			} catch (Exception e) {
				logger.info("Exception deleteParticipant:", e);
				return null;
			}
		}

		public List<ParticipantModel> getParticipants() throws Exception {
			try {
				List<Participant> participants = participantDAO.getParticipants();
				List<ParticipantModel> participantModels = participantMapper.entityList(participants);
				return participantModels;
			} catch (Exception ex) {
				logger.info("Exception getParticipants:", ex);
			}
			return null;
		}

		public ParticipantModel authenticateP(ParticipantModel participantModel) throws Exception {
			participantModel.setPassword(CommonUtils.encriptString(participantModel.getPassword()));
			Participant participant = new Participant();
			BeanUtils.copyProperties(participantModel, participant);
			participant = participantDAO.authenticateP(participant);
			if (participant == null)
				return null;
			BeanUtils.copyProperties(participant, participantModel);
			return participantModel;
		}


		public boolean isParticipantNameExist(String userName) {
			try {
				return participantDAO.isParticipantNameExist(userName);
			} catch (Exception e) {
				logger.info("Exception isParticipantNameExist:", e);
				return false;
			}
		}

		@Override
		public Response updateParticipant(ParticipantModel participantModel) {
			try {
				Participant participant = new Participant();
				BeanUtils.copyProperties(participantModel, participant);
				Response response = participantDAO.updateParticipant(participant);
				return response;
			} catch (Exception ex) {
				logger.info("Exception Service:" + ex.getMessage());
			}
			return null;
		}

		
		
		
		public Response saveBhost(BHostModel bhostModel) {
			try {
				BusinessHost bhost = new BusinessHost();
				BeanUtils.copyProperties(bhostModel, bhost);
				bhost.setBhId(CommonUtils.getRandomUUID());
				bhost.setPassword(CommonUtils.encriptString(bhost.getPassword()));
				bhost.setIsActive(true);
				Response response = bhostDAO.saveBhost(bhost);
				return response;
			} catch (Exception ex) {
				logger.info("Exception Service:" + ex.getMessage());
			}
			return null;
		}

		public BHostModel getBhost(String bhId) {
			try {
				BusinessHost bhost = bhostDAO.getBhost(bhId);
				BHostModel bhostModel = new BHostModel();
				if (bhost == null)
					return null;
				BeanUtils.copyProperties(bhost, bhostModel);
				return bhostModel;
			} catch (Exception e) {
				logger.info("Exception getBHost:", e);
				return null;
			}
		}

		public Response deleteBhost(String bhId) {
			try {
				return bhostDAO.deleteBhost(bhId);
			} catch (Exception e) {
				logger.info("Exception deleteBhost:", e);
				return null;
			}
		}

		public List<BHostModel> getBhosts() throws Exception {
			try {
				List<BusinessHost> bhosts = bhostDAO.getBhosts();
				List<BHostModel> bhostModels = bhostMapper.entityList(bhosts);
				return bhostModels;
			} catch (Exception ex) {
				logger.info("Exception getBhosts:", ex);
			}
			return null;
		}

		public BHostModel authenticateBhost(BHostModel bhostModel) throws Exception {
			bhostModel.setPassword(CommonUtils.encriptString(bhostModel.getPassword()));
			BusinessHost bhost = new BusinessHost();
			BeanUtils.copyProperties(bhostModel, bhost);
			bhost = bhostDAO.authenticateBhost(bhost);
			if (bhost == null)
				return null;
			BeanUtils.copyProperties(bhost, bhostModel);
			return bhostModel;
		}


		public boolean isBhostNameExist(String userName) {
			try {
				return bhostDAO.isBhostNameExist(userName);
			} catch (Exception e) {
				logger.info("Exception isBhostNameExist:", e);
				return false;
			}
		}

		@Override
		public Response updateBhost(BHostModel bhostModel) {
			try {
				BusinessHost bhost = new BusinessHost();
				BeanUtils.copyProperties(bhostModel, bhost);
				Response response = bhostDAO.updateBhost(bhost);
				return response;
			} catch (Exception ex) {
				logger.info("Exception Service:" + ex.getMessage());
			}
			return null;
		}


		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public Response saveIhost(IHostModel ihostModel) {
			try {
				IndividualHost ihost = new IndividualHost();
				BeanUtils.copyProperties(ihostModel, ihost);
				ihost.setIhId(CommonUtils.getRandomUUID());
				ihost.setPassword(CommonUtils.encriptString(ihost.getPassword()));
				ihost.setIsActive(true);
				Response response = ihostDAO.saveIhost(ihost);
				return response;
			} catch (Exception ex) {
				logger.info("Exception Service:" + ex.getMessage());
			}
			return null;
		}

		public IHostModel getIhost(String ihId) {
			try {
				IndividualHost ihost = ihostDAO.getIhost(ihId);
				IHostModel ihostModel = new IHostModel();
				if (ihost == null)
					return null;
				BeanUtils.copyProperties(ihost, ihostModel);
				return ihostModel;
			} catch (Exception e) {
				logger.info("Exception getIhost:", e);
				return null;
			}
		}

		public Response deleteIhost(String ihId) {
			try {
				return ihostDAO.deleteIhost(ihId);
			} catch (Exception e) {
				logger.info("Exception deleteIhost:", e);
				return null;
			}
		}

		public List<IHostModel> getIhosts() throws Exception {
			try {
				List<IndividualHost> ihosts = ihostDAO.getIhosts();
				List<IHostModel> ihostModels = ihostMapper.entityList(ihosts);
				return ihostModels;
			} catch (Exception ex) {
				logger.info("Exception getIhosts:", ex);
			}
			return null;
		}

		public IHostModel authenticateIhost(IHostModel ihostModel) throws Exception {
			ihostModel.setPassword(CommonUtils.encriptString(ihostModel.getPassword()));
			IndividualHost ihost = new IndividualHost();
			BeanUtils.copyProperties(ihostModel, ihost);
			ihost = ihostDAO.authenticateIhost(ihost);
			if (ihost == null)
				return null;
			BeanUtils.copyProperties(ihost, ihostModel);
			return ihostModel;
		}


		public boolean isIhostNameExist(String userName) {
			try {
				return ihostDAO.isIhostNameExist(userName);
			} catch (Exception e) {
				logger.info("Exception isIhostNameExist:", e);
				return false;
			}
		}

		@Override
		public Response updateIhost(IHostModel ihostModel) {
			try {
				IndividualHost ihost = new IndividualHost();
				BeanUtils.copyProperties(ihostModel, ihost);
				Response response = ihostDAO.updateIhost(ihost);
				return response;
			} catch (Exception ex) {
				logger.info("Exception Service:" + ex.getMessage());
			}
			return null;
		}

		
		


}
