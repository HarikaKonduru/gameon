package com.snipe.gameon.service.users;

import java.util.List;

import com.snipe.gameon.model.users.IHostModel;
import com.snipe.gameon.response.Response;

public interface IHostService {
	public Response saveIhost(IHostModel ihost) throws Exception;

	public Response updateIhost(IHostModel ihost) throws Exception;

	public Response deleteIhost(String ihId) throws Exception;

	public IHostModel getIhost(String ihId) throws Exception;
	
	public List<IHostModel> getIhosts() throws Exception;

	public boolean isIhostNameExist(String userName) throws Exception;

	public IHostModel authenticateIhost(IHostModel ihost) throws Exception;
}
