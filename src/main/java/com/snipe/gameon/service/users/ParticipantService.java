package com.snipe.gameon.service.users;

import java.util.List;

import com.snipe.gameon.model.users.ParticipantModel;
import com.snipe.gameon.response.Response;

public interface ParticipantService {
	
	public Response saveParticpant(ParticipantModel participant) throws Exception;

	public Response updateParticipant(ParticipantModel participant) throws Exception;

	public Response deleteParticipant(String participantId) throws Exception;

	public ParticipantModel getParticipant(String participantId) throws Exception;

	public List<ParticipantModel> getParticipants() throws Exception;

	public boolean isParticipantNameExist(String userName) throws Exception;

	public ParticipantModel authenticateP(ParticipantModel participant) throws Exception;
	

}
