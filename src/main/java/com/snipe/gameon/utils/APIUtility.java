package com.snipe.gameon.utils;


	import java.nio.charset.StandardCharsets;
	import java.util.Iterator;
	import java.util.Map;

	import org.apache.http.client.config.RequestConfig;
	import org.apache.http.client.methods.CloseableHttpResponse;
	import org.apache.http.client.methods.HttpPost;
	import org.apache.http.client.methods.HttpRequestBase;
	import org.apache.http.entity.StringEntity;
	import org.apache.http.impl.client.CloseableHttpClient;
	import org.apache.http.impl.client.HttpClientBuilder;
	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
	import org.springframework.stereotype.Component;

	@Component
	public class APIUtility {
		private static final Logger logger = LoggerFactory.getLogger(APIUtility.class);

		public static CloseableHttpResponse doHttpPost(final String url, Map<String, String> headers,
				final String content) {
			CloseableHttpResponse httpResponse = null;
			try {
				logger.info("json request : " + content);
				logger.info("url--> : " + url);
				final HttpClientBuilder hcb = HttpClientBuilder.create();
				final CloseableHttpClient client = hcb.build();

				RequestConfig rc = RequestConfig.custom().setConnectTimeout(60000).setSocketTimeout(60000).build();

				HttpRequestBase hrb = new HttpPost(url);
				hrb.setConfig(rc);
				if (headers != null) {
					Iterator<Map.Entry<String, String>> headersIterator = headers.entrySet().iterator();
					while (headersIterator.hasNext()) {
						Map.Entry<String, String> e = headersIterator.next();
						hrb.setHeader(e.getKey(), e.getValue());
					}
				}
				try {
					if (content != null) {
						((HttpPost) hrb).setEntity(new StringEntity(content, StandardCharsets.UTF_8.name()));
					}
					httpResponse = client.execute(hrb);
				} catch (Exception e) {
					logger.error("Exception in doHttpPost : ", e);
				} finally {
					// hrb.releaseConnection();
				}
			} catch (Exception e) {
				logger.error("Exception in doHttpPost : ", e);
			}
			return httpResponse;
		}
	

}
