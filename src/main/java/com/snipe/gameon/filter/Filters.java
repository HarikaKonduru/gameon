package com.snipe.gameon.filter;


	import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.snipe.gameon.config.GameOnProperties;
import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.constants.StatusCode;
import com.snipe.gameon.response.ErrorObject;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.utils.CommonUtils;

	

	@Component
	@Order(Ordered.HIGHEST_PRECEDENCE)
	@ComponentScan(basePackages = { "com.etree.dictionary" })
	public class Filters implements Constants, Filter {
		@Autowired
		GameOnProperties gameonProperties;
		private static final Logger logger = LoggerFactory.getLogger(Filters.class);

		@SuppressWarnings("unused")
		public void init(FilterConfig config) throws ServletException {
			WebApplicationContext springContext = WebApplicationContextUtils
					.getWebApplicationContext(config.getServletContext());
		}

		public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
				throws IOException, ServletException {
			logger.debug("In doFilter");
			final HttpServletRequest request = (HttpServletRequest) req;
			final HttpServletResponse response = (HttpServletResponse) res;
			cors(request, response);
			String url = request.getRequestURL().toString();
			if (url.contains("swagger") || url.contains("api-docs") || url.contains("image/stock")
					|| url.contains("image/order")) {
				chain.doFilter(request, response);
			} else {
				String xApiKey = request.getHeader(ACCESS_KEY_HEADER);
				String jsonResponse = null;
				jsonResponse = validateRequest(xApiKey);
				if (jsonResponse != null) {
					response.getWriter().println(jsonResponse);
				} else {
					chain.doFilter(request, response);
				}
			}
		}

		public void destroy() {
		}

		public String validateRequest(String xApiKey) {
			Response response = new Response();
			ArrayList<ErrorObject> errorObjects = new ArrayList<ErrorObject>();
			if (!gameonProperties.getHeaderApiKey().equalsIgnoreCase(xApiKey)) {
				ErrorObject errorObject = new ErrorObject();
				errorObject.setTitle("Invalid Header Api Key");
				errorObject.setDetail("Invalid Header Api Key");
				errorObjects.add(errorObject);
			}
			if (errorObjects != null && !errorObjects.isEmpty()) {
				response.setStatus(StatusCode.ERROR.name());
				response.setErrors((ErrorObject[]) errorObjects.toArray(new ErrorObject[errorObjects.size()]));
			} else {
				response.setStatus(StatusCode.SUCCESS.name());
			}
			if (response != null && response.getStatus().equalsIgnoreCase(StatusCode.ERROR.name()))
				return CommonUtils.getJson(response);
			return null;

		}

		public void cors(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

			response.addHeader("Access-Control-Allow-Origin", "*");
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH, HEAD, OPTIONS");
			response.addHeader("Access-Control-Allow-Headers",
					"Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-API-KEY");
			response.addHeader("Access-Control-Expose-Headers",
					"Access-Control-Allow-Origin, Access-Control-Allow-Credentials");
			response.addHeader("Access-Control-Allow-Credentials", "true");
			response.addIntHeader("Access-Control-Max-Age", 3600);

		}

	
}
