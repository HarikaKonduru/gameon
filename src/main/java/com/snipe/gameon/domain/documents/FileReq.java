package com.snipe.gameon.domain.documents;


	import java.io.Serializable;

	public class FileReq implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8672542204400828583L;
		private String id;
		private String languageCode;
		private String surveyName;
		private String surveyDesc;
		private String filePath;
		private boolean isActive;
		private String startDate;
		private String endDate;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getLanguageCode() {
			return languageCode;
		}

		public void setLanguageCode(String languageCode) {
			this.languageCode = languageCode;
		}

		public String getSurveyName() {
			return surveyName;
		}

		public void setSurveyName(String surveyName) {
			this.surveyName = surveyName;
		}

		public String getSurveyDesc() {
			return surveyDesc;
		}

		public void setSurveyDesc(String surveyDesc) {
			this.surveyDesc = surveyDesc;
		}

		public String getFilePath() {
			return filePath;
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		public boolean getIsActive() {
			return isActive;
		}

		public void setIsActive(boolean isActive) {
			this.isActive = isActive;
		}

	}
