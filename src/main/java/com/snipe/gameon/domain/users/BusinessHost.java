package com.snipe.gameon.domain.users;

import java.util.Date;

public class BusinessHost {
		
		private String bhId;
		
		private String type;
		private String userName;
		private String companyName;
		private String businessPhone;
		private String cellPhone;
		private String password;
		private String email;
		private String contactPerson;
		private String eventType;
		private String address;
		private String notification;
		private String venue;
		private String adharNumber;
		private boolean isActive;
		private Date createdDate;
		private Date modifiedDate;
		
		
		public Date getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}

		public Date getModifiedDate() {
			return modifiedDate;
		}

		public void setModifiedDate(Date modifiedDate) {
			this.modifiedDate = modifiedDate;
		}

		public BusinessHost() {
			
		}
		
		public String getBhId() {
			return bhId;
		}
		public void setBhId(String bhId) {
			this.bhId = bhId;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getCompanyName() {
			return companyName;
		}
		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		public String getBusinessPhone() {
			return businessPhone;
		}
		public void setBusinessPhone(String businessPhone) {
			this.businessPhone = businessPhone;
		}
		public String getCellPhone() {
			return cellPhone;
		}
		public void setCellPhone(String cellPhone) {
			this.cellPhone = cellPhone;
		}
		
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getContactPerson() {
			return contactPerson;
		}
		public void setContactPerson(String contactPerson) {
			this.contactPerson = contactPerson;
		}
		public String getEventType() {
			return eventType;
		}
		public void setEventType(String eventType) {
			this.eventType = eventType;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getNotification() {
			return notification;
		}
		public void setNotification(String notification) {
			this.notification = notification;
		}
		public String getVenue() {
			return venue;
		}
		public void setVenue(String venue) {
			this.venue = venue;
		}
		public String getAdharNumber() {
			return adharNumber;
		}
		public void setAdharNumber(String adharNumber) {
			this.adharNumber = adharNumber;
		}

		

		public boolean isActive() {
			return isActive;
		}

		public void setIsActive(boolean isActive) {
			this.isActive = isActive;
		}
		

}
