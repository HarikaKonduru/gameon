package com.snipe.gameon.model.users;

import java.util.Date;

public class ParticipantModel {
		
		
			private String participantId;
			private String userName;
			private String firstName;
			private String lastName;
			private String password;

			private String gender;
			private String phoneNumber;
			private String email;
			private String notification;
			private boolean isActive;	
			private String events;
			private String rating;
			private String favouriteEventers;
			private String interests;
			private Date createdDate;
			private Date modifiedDate;
			
			
			public Date getCreatedDate() {
				return createdDate;
			}

			public void setCreatedDate(Date createdDate) {
				this.createdDate = createdDate;
			}

			public Date getModifiedDate() {
				return modifiedDate;
			}

			public void setModifiedDate(Date modifiedDate) {
				this.modifiedDate = modifiedDate;
			}

			public String getParticipantId() {
				return participantId;
			}

			public void setParticipantId(String participantId) {
				this. participantId =  participantId;
			}
			

			public String getUserName() {
				return userName;
			}

			public void setUserName(String userName) {
				this.userName = userName;
			}

			public String getFirstName() {
				return firstName;
			}

			public void setFirstName(String firstName) {
				this.firstName = firstName;
			}

			public String getLastName() {
				return lastName;
			}

			public void setLastName(String lastName) {
				this.lastName = lastName;
			}

			public String getGender() {
				return gender;
			}

			public void setGender(String gender) {
				this.gender = gender;
			}

			public String getPhoneNumber() {
				return phoneNumber;
			}

			public void setPhoneNumber(String phoneNumber) {
				this.phoneNumber = phoneNumber;
			}

			public String getEmail() {
				return email;
			}

			public void setEmail(String email) {
				this.email = email;
			}

			public String getNotification() {
				return notification;
			}

			public void setNotification(String notification) {
				this.notification = notification;
			}

			public String getEvents() {
				return events;
			}

			public void setEvents(String events) {
				this.events = events;
			}

			public String getRating() {
				return rating;
			}

			public void setRating(String rating) {
				this.rating = rating;
			}
			public String getPassword() {
				return password;
			}

			public void setPassword(String password) {
				this.password = password;
			}
			public String getFavouriteEventers() {
				return favouriteEventers;
			}

			public void setFavouriteEventers(String favouriteEventers) {
				this.favouriteEventers = favouriteEventers;
			}

			public String getInterests() {
				return interests;
			}

			public void setInterests(String interests) {
				this.interests = interests;
			}

			public boolean isActive() {
				return isActive;
			}

			public void setActive(boolean isActive) {
				this.isActive = isActive;
			}
			
			

			
		

	

}
