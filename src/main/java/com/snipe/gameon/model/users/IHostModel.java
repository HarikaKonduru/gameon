package com.snipe.gameon.model.users;




	import java.util.Date;

	
	public class IHostModel {
		
		
		private String ihId;
		private String type;
		private String userName;
		private String firstName;
		private String lastName;
		private String password;
		private String gender;
		private Date dob;
		private String phoneNumber;
		private String mobileNumber;
		private String eventType;
		private String address;
		private String notification;
		private String rating;
		private String eventName;
		private String eventDescription;
		private String hostedEvents;
		private Date dateOfEvent;
		private String trackInterests;
		private boolean isActive;
		private Date createdDate;
		private Date modifiedDate;
		
		
		public Date getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		public Date getModifiedDate() {
			return modifiedDate;
		}
		public void setModifiedDate(Date modifiedDate) {
			this.modifiedDate = modifiedDate;
		}
		public String getIhId() {
			return ihId;
		}
		public void setIhId(String ihId) {
			this.ihId = ihId;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public Date getDob() {
			return dob;
		}
		public void setDob(Date dob) {
			this.dob = dob;
		}
		
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getMobileNumber() {
			return mobileNumber;
		}
		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getEventType() {
			return eventType;
		}
		public void setEventType(String eventType) {
			this.eventType = eventType;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getNotification() {
			return notification;
		}
		public void setNotification(String notification) {
			this.notification = notification;
		}
		public String getRating() {
			return rating;
		}
		public void setRating(String rating) {
			this.rating = rating;
		}
		
		public String getEventName() {
			return eventName;
		}
		public void setEventName(String eventName) {
			this.eventName = eventName;
		}
		public String getEventDescription() {
			return eventDescription;
		}
		public void setEventDescription(String eventDescription) {
			this.eventDescription = eventDescription;
		}
		public String getHostedEvents() {
			return hostedEvents;
		}
		public void setHostedEvents(String hostedEvents) {
			this.hostedEvents = hostedEvents;
		}
		public Date getDateOfEvent() {
			return dateOfEvent;
		}
		public void setDateOfEvent(Date dateOfEvent) {
			this.dateOfEvent = dateOfEvent;
		}
		public String getTrackInterests() {
			return trackInterests;
		}
		public void setTrackInterests(String trackInterests) {
			this.trackInterests = trackInterests;
		}
		public boolean isActive() {
			return isActive;
		}
		public void setActive(boolean isActive) {
			this.isActive = isActive;
		}
		
		
	




}
