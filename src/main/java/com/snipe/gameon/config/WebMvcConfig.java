package com.snipe.gameon.config;

import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

public class WebMvcConfig extends WebMvcConfigurationSupport {
	 @Override
	    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
	        configurer.setDefaultTimeout(1000000);
	    }
}
