package com.snipe.gameon.config;


	import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

import com.fasterxml.jackson.databind.ObjectMapper;

	@Configuration
	@EnableWebSocketMessageBroker
	public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

		@Autowired ObjectMapper objectMapper;
		
	    @Override
	    public void configureMessageBroker(MessageBrokerRegistry config) {
	        config.enableSimpleBroker("/topic");
	        config.setApplicationDestinationPrefixes("/app");
	    }

	    @Override
	    public void registerStompEndpoints(StompEndpointRegistry registry) {
	        registry.addEndpoint("/gs-guide-websocket").withSockJS();
	    }
	    
	    @Override 
	    public void configureClientInboundChannel(ChannelRegistration registration) { 
	    } 
	 
	    @Override 
	    public void configureClientOutboundChannel(ChannelRegistration registration) { 
	        registration.taskExecutor().corePoolSize(4).maxPoolSize(10); 
	    }
	   

		@Override
		public void configureWebSocketTransport(WebSocketTransportRegistration registry) {
			// TODO Auto-generated method stub
			
		} 

	
}
