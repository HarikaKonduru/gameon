package com.snipe.gameon.config;

	import java.io.Serializable;

	import org.springframework.boot.context.properties.ConfigurationProperties;
	import org.springframework.stereotype.Component;

	@Component
	@ConfigurationProperties("gameon")
	public class RTSProperties implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1104118810026296091L;
		private String headerApiKey;
		private String toEmail;
		private String fromEmail;
		private String toCC;
		private String toBCC;
		public String getHeaderApiKey() {
			return headerApiKey;
		}
		public void setHeaderApiKey(String headerApiKey) {
			this.headerApiKey = headerApiKey;
		}
		public String getToEmail() {
			return toEmail;
		}
		public void setToEmail(String toEmail) {
			this.toEmail = toEmail;
		}
		public String getFromEmail() {
			return fromEmail;
		}
		public void setFromEmail(String fromEmail) {
			this.fromEmail = fromEmail;
		}
		public String getToCC() {
			return toCC;
		}
		public void setToCC(String toCC) {
			this.toCC = toCC;
		}
		public String getToBCC() {
			return toBCC;
		}
		public void setToBCC(String toBCC) {
			this.toBCC = toBCC;
		}
		
		
	
}
