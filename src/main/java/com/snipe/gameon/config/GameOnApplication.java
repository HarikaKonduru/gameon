package com.snipe.gameon.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication(scanBasePackages = { "com.snipe.gameon" })
@EnableConfigurationProperties({
	FileStorageProperties.class
})
public class GameOnApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameOnApplication.class, args);
	}
}
