package com.snipe.gameon.mapper.users;



	import org.springframework.stereotype.Component;

import com.snipe.gameon.domain.users.BusinessHost;
import com.snipe.gameon.mapper.AbstractModelMapper;
import com.snipe.gameon.model.users.BHostModel;

	
	

	@Component
	public class BHostMapper extends AbstractModelMapper<BHostModel, BusinessHost> {

		@Override
		public Class<BHostModel> entityType() {
			return BHostModel.class;
		}

		@Override
		public Class<BusinessHost> modelType() {
			return BusinessHost.class;
		}

	

}
