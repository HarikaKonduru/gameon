package com.snipe.gameon.mapper.users;



	import org.springframework.stereotype.Component;

import com.snipe.gameon.domain.users.Participant;
import com.snipe.gameon.mapper.AbstractModelMapper;
import com.snipe.gameon.model.users.ParticipantModel;

	

	@Component
	public class ParticipantMapper extends AbstractModelMapper<ParticipantModel, Participant> {

		@Override
		public Class<ParticipantModel> entityType() {
			return ParticipantModel.class;
		}

		@Override
		public Class<Participant> modelType() {
			return Participant.class;
		}

	

}
