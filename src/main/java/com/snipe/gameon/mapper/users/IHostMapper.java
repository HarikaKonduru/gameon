package com.snipe.gameon.mapper.users;




	import org.springframework.stereotype.Component;

import com.snipe.gameon.domain.users.IndividualHost;
import com.snipe.gameon.mapper.AbstractModelMapper;
import com.snipe.gameon.model.users.IHostModel;

	
	

	@Component
	public class IHostMapper extends AbstractModelMapper<IHostModel, IndividualHost> {

		@Override
		public Class<IHostModel> entityType() {
			return IHostModel.class;
		}

		@Override
		public Class<IndividualHost> modelType() {
			return IndividualHost.class;
		}

	

}


