package com.snipe.gameon.constants;

public enum FileType {
	PDF,JPEG,PNG,DOCX
}
