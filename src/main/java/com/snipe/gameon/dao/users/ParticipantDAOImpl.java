package com.snipe.gameon.dao.users;


	

	import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.constants.StatusCode;
import com.snipe.gameon.domain.users.Participant;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.utils.CommonUtils;

	

	@Repository
	public class ParticipantDAOImpl implements ParticipantDAO, Constants {
		private static final Logger logger = LoggerFactory.getLogger(ParticipantDAOImpl.class);
		@Autowired
		JdbcTemplate jdbcTemplate;

		public Response saveParticipant(Participant participant) {
			Response response = CommonUtils.getResponseObject("Add participant data");
			try {
				String sql = "INSERT INTO participant (participantId,userName,firstName,lastName,password,gender,mobile,email,notification,events,rating,favouriteEventers,interests,isActive,createdDate,modifiedDate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				int res = jdbcTemplate.update(sql,
						new Object[] { participant.getParticipantId(), participant.getUserName(), participant.getFirstName(), participant.getLastName(),
						participant.getPassword(), participant.getEmail(), participant.getPhoneNumber(), participant.getNotification(), participant.getEvents(), participant.getFavouriteEventers(), participant.getInterests(),
						participant.getGender(), participant.getRating(),participant.isActive(),participant.getModifiedDate(),participant.getCreatedDate()});
				if (res == 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in addParticipant", e);
				return CommonUtils.getDuplicateKeyMessage(response, e);
			}
			return response;
		}

		public Response updateParticipant(Participant participant) {
			Response response = CommonUtils.getResponseObject("Update participant data");
			try {
				String sql = "UPDATE participant SET userName=?,firstName=?,lastName=?,gender=?, email=?,mobile=?,notification=?, events=?, rating=?,favouriteEventers=?, interests=?, isActive=?, createdDate=?, modifiedDate=?  WHERE participantId=?";
				int res = jdbcTemplate.update(sql, participant.getParticipantId(), participant.getUserName(), participant.getFirstName(), participant.getLastName(),
					 participant.getEmail(), participant.getPhoneNumber(), participant.getNotification(), participant.getEvents(), participant.getFavouriteEventers(), participant.getInterests(),
						participant.getGender(), participant.getRating(),participant.getCreatedDate(),participant.isActive(),participant.getModifiedDate());
				if (res == 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in updateParticipant", e);
				return CommonUtils.getDuplicateKeyMessage(response, e);
			}
			return response;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public Participant getParticipant(String participantId) {
			try {
				String sql = "SELECT * FROM participant where participantId=?";
				return (Participant) jdbcTemplate.queryForObject(sql, new Object[] { participantId },
						new BeanPropertyRowMapper(Participant.class));
			} catch (EmptyResultDataAccessException e) {
				return null;
			} catch (Exception e) {
				logger.error("Exception in getParticipant", e);
				return null;
			}
		}

		public List<Participant> getParticipants() throws Exception {
			try {
				String sql = "SELECT * FROM participant";
				List<Participant> participants = jdbcTemplate.query(sql, new Object[] {}, new BeanPropertyRowMapper<Participant>(Participant.class));
				return participants;
			} catch (Exception e) {
				logger.error("Exception in getParticipants", e);
			}
			return null;
		}

		@Override
		public boolean isParticipantNameExist(String userName) {
			try {
				String sql = "SELECT count(userName) FROM participant WHERE userName=?";
				int count = jdbcTemplate.queryForObject(sql, new Object[] { userName }, Integer.class);
				boolean isExist = count > 0 ? true : false;
				return isExist;
			} catch (Exception e) {
				logger.error("Exception in isParticipantNameExist: ", e);
			}
			return false;
		}

		@Override
		public boolean isParticipantExist(String participantId) {
			try {
				String sql = "SELECT count(participantId) FROM participant WHERE participantId=?";
				int count = jdbcTemplate.queryForObject(sql, new Object[] { participantId }, Integer.class);
				boolean isExist = count > 0 ? true : false;
				return isExist;
			} catch (Exception e) {
				logger.error("Exception in isParticipantExist: ", e);
			}
			return false;
		}

		@Override
		public Response deleteParticipant(String participantId) throws Exception {
			Response response = CommonUtils.getResponseObject("Delete participant data");
			try {
				String sql = "delete from partiicpant WHERE participantId=?";
				int rows = jdbcTemplate.update(sql, participantId);
				if (rows > 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in deleteParticipant", e);
				response.setStatus(StatusCode.ERROR.name());
				response.setErrors(e.getMessage());
			}
			return response;
		}

		@Override
		public Participant authenticateP(Participant participant) throws Exception {
			try {
				String sql = "SELECT * FROM participant where userName=? and password=? and isActive=1";
				return (Participant) jdbcTemplate.queryForObject(sql, new Object[] { participant.getUserName(), participant.getPassword() },
						new BeanPropertyRowMapper<Participant>(Participant.class));
			} catch (Exception e) {
				logger.error("Exception in authenticate", e);
			}
			return null;
		}

		

		

		
	
}
