package com.snipe.gameon.dao.users;

import java.util.List;

import com.snipe.gameon.domain.users.IndividualHost;
import com.snipe.gameon.response.Response;

public interface IHostDAO {
	
	public Response saveIhost(IndividualHost ihost) throws Exception;

	public Response updateIhost(IndividualHost ihost) throws Exception;

	public Response deleteIhost(String ihId) throws Exception;

	public IndividualHost getIhost(String ihId) throws Exception;

	public List<IndividualHost> getIhosts() throws Exception;

	public IndividualHost authenticateIhost(IndividualHost ihost) throws Exception;

	public boolean isIhostNameExist(String userName) throws Exception;

	public boolean isIhostExist(String ihId) throws Exception;
}
