package com.snipe.gameon.dao.users;



	import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.constants.StatusCode;
import com.snipe.gameon.domain.users.IndividualHost;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.utils.CommonUtils;

	

	@Repository
	public class IHostDAOImpl implements IHostDAO, Constants {
		private static final Logger logger = LoggerFactory.getLogger(IHostDAOImpl.class);
		@Autowired
		JdbcTemplate jdbcTemplate;

		public Response saveIhost(IndividualHost ihost) {
			Response response = CommonUtils.getResponseObject("Add host data");
			try {
				String sql = "INSERT INTO IndividualHost (ihId, type, userName, firstName, lastName, password, gender,dob, phoneNumber, mobileNumber,eventType,address, notification,rating, eventName, eventDescription, hostedEvents, dateOfEvent, trackInterests,isActive,createdDate,modifiedDate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				int res = jdbcTemplate.update(sql,
						new Object[] { ihost.getIhId(),ihost.getType(), ihost.getUserName(), ihost.getFirstName(),ihost.getLastName(),
								ihost.getPassword(),ihost.getGender(), ihost.getMobileNumber(), ihost.getPhoneNumber(),
								ihost.getEventType(),ihost.getAddress(),ihost.getNotification(),ihost.getDob(),
								ihost.getRating(),ihost.getEventName(),ihost.getEventDescription(),ihost.getHostedEvents(),ihost.getHostedEvents(),
								ihost.getDateOfEvent(),ihost.getTrackInterests(),
				ihost.isActive(),ihost.getModifiedDate(),ihost.getCreatedDate()});
				if (res == 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in addIhost", e);
				return CommonUtils.getDuplicateKeyMessage(response, e);
			}
			return response;
		}

		public Response updateIhost(IndividualHost ihost) {
			Response response = CommonUtils.getResponseObject("Update host data");
			try {
				String sql = "UPDATE ihost SET userName=?,type=?,firstName=?, lastName=?,gender=?,dob=?, phoneNumber=?, mobileNumber=?,eventType=?,address=?, notification=?,rating=?,eventName=?,eventDescription=?,hostedEvents=?,dateOfEvents=?,trackInterests=? createdDate=?, modifiedDate=?  WHERE ihId=?";
				int res = jdbcTemplate.update(sql, ihost.getType(), ihost.getUserName(), ihost.getFirstName(),ihost.getLastName(),
						ihost.getGender(), ihost.getMobileNumber(), ihost.getPhoneNumber(),
						ihost.getEventType(),ihost.getAddress(),ihost.getNotification(),ihost.getDob(),
						ihost.getRating(),ihost.getEventName(),ihost.getEventDescription(),ihost.getHostedEvents(),ihost.getHostedEvents(),
						ihost.getDateOfEvent(),ihost.getTrackInterests(),
						ihost.getCreatedDate(),ihost.isActive(),ihost.getModifiedDate(),ihost.getIhId());
				if (res == 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in updateIhost", e);
				return CommonUtils.getDuplicateKeyMessage(response, e);
			}
			return response;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public IndividualHost getIhost(String ihId) {
			try {
				String sql = "SELECT * FROM ihost where ihId=?";
				return (IndividualHost) jdbcTemplate.queryForObject(sql, new Object[] { ihId },
						new BeanPropertyRowMapper(IndividualHost.class));
			} catch (EmptyResultDataAccessException e) {
				return null;
			} catch (Exception e) {
				logger.error("Exception in getihost", e);
				return null;
			}
		}

		public List<IndividualHost> getIhosts() throws Exception {
			try {
				String sql = "SELECT * FROM ihost";
				List<IndividualHost> ihosts = jdbcTemplate.query(sql, new Object[] {}, new BeanPropertyRowMapper<IndividualHost>(IndividualHost.class));
				return ihosts;
			} catch (Exception e) {
				logger.error("Exception in getIhosts", e);
			}
			return null;
		}

		@Override
		public boolean isIhostNameExist(String userName) {
			try {
				String sql = "SELECT count(userName) FROM ihost WHERE userName=?";
				int count = jdbcTemplate.queryForObject(sql, new Object[] { userName }, Integer.class);
				boolean isExist = count > 0 ? true : false;
				return isExist;
			} catch (Exception e) {
				logger.error("Exception in isIhostNameExist: ", e);
			}
			return false;
		}

		@Override
		public boolean isIhostExist(String ihId) {
			try {
				String sql = "SELECT count(ihId) FROM ihost WHERE ihId=?";
				int count = jdbcTemplate.queryForObject(sql, new Object[] {ihId }, Integer.class);
				boolean isExist = count > 0 ? true : false;
				return isExist;
			} catch (Exception e) {
				logger.error("Exception in isIhostExist: ", e);
			}
			return false;
		}

		@Override
		public Response deleteIhost(String ihId) throws Exception {
			Response response = CommonUtils.getResponseObject("Delete host data");
			try {
				String sql = "delete from ihost WHERE ihId=?";
				int rows = jdbcTemplate.update(sql, ihId);
				if (rows > 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in deleteIhost", e);
				response.setStatus(StatusCode.ERROR.name());
				response.setErrors(e.getMessage());
			}
			return response;
		}

		@Override
		public IndividualHost authenticateIhost(IndividualHost ihost) throws Exception {
			try {
				String sql = "SELECT * FROM ihost where userName=? and password=? and isActive=1";
				return (IndividualHost) jdbcTemplate.queryForObject(sql, new Object[] { ihost.getUserName(), ihost.getPassword() },
						new BeanPropertyRowMapper<IndividualHost>(IndividualHost.class));
			} catch (Exception e) {
				logger.error("Exception in authenticate", e);
			}
			return null;
		}

		

		
		

		

		

		
	


}
