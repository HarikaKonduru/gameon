package com.snipe.gameon.dao.users;


	import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.snipe.gameon.constants.Constants;
import com.snipe.gameon.constants.StatusCode;
import com.snipe.gameon.domain.users.BusinessHost;
import com.snipe.gameon.response.Response;
import com.snipe.gameon.utils.CommonUtils;

	

	@Repository
	public class BHostDAOImpl implements BHostDAO, Constants {
		private static final Logger logger = LoggerFactory.getLogger(BHostDAOImpl.class);
		@Autowired
		JdbcTemplate jdbcTemplate;

		public Response saveBhost(BusinessHost bhost) {
			Response response = CommonUtils.getResponseObject("Add host data");
			try {
				String sql = "INSERT INTO bhost (bhId,type,userName,companyName,password,email,businessPhone,cellPhone,contactPerson,eventType,address,notification,venue,adharNumber,isActive,createdDate,modifiedDate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				int res = jdbcTemplate.update(sql,
						new Object[] { bhost.getBhId(),bhost.getType(), bhost.getUserName(), bhost.getCompanyName(),
								bhost.getPassword(),bhost.getEmail(), bhost.getBusinessPhone(), bhost.getCellPhone(),bhost.getContactPerson(),bhost.getEventType(),bhost.getAddress(),bhost.getNotification(),bhost.getVenue(),
								bhost.getAdharNumber(),bhost.isActive(),bhost.getModifiedDate(),bhost.getCreatedDate()});
				if (res == 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in addIhost", e);
				return CommonUtils.getDuplicateKeyMessage(response, e);
			}
			return response;
		}

		public Response updateBhost(BusinessHost bhost) {
			Response response = CommonUtils.getResponseObject("Update host data");
			try {
				String sql = "UPDATE bhost SET userName=?,campanyName=?,email=?,businessPhone=?,cellPhone=?,contactPerson=?,eventType=?,address=?,notification=?,venue=?,isActive=?, createdDate=?, modifiedDate=?  WHERE bhId=?";
				int res = jdbcTemplate.update(sql, bhost.getUserName(), bhost.getCompanyName()
						,bhost.getEmail(), bhost.getBusinessPhone(), bhost.getCellPhone(),bhost.getContactPerson(),bhost.getEventType(),bhost.getAddress(),bhost.getNotification(),bhost.getVenue(), bhost.getCreatedDate(),bhost.isActive(),bhost.getModifiedDate(),bhost.getBhId());
				if (res == 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in updateBhost", e);
				return CommonUtils.getDuplicateKeyMessage(response, e);
			}
			return response;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public BusinessHost getBhost(String bhId) {
			try {
				String sql = "SELECT * FROM bhost where bhId=?";
				return (BusinessHost) jdbcTemplate.queryForObject(sql, new Object[] { bhId },
						new BeanPropertyRowMapper(BusinessHost.class));
			} catch (EmptyResultDataAccessException e) {
				return null;
			} catch (Exception e) {
				logger.error("Exception in getBhost", e);
				return null;
			}
		}

		public List<BusinessHost> getBhosts() throws Exception {
			try {
				String sql = "SELECT * FROM bhost";
				List<BusinessHost> bhosts = jdbcTemplate.query(sql, new Object[] {}, new BeanPropertyRowMapper<BusinessHost>(BusinessHost.class));
				return bhosts;
			} catch (Exception e) {
				logger.error("Exception in getBhosts", e);
			}
			return null;
		}

		@Override
		public boolean isBhostNameExist(String userName) {
			try {
				String sql = "SELECT count(userName) FROM bhost WHERE userName=?";
				int count = jdbcTemplate.queryForObject(sql, new Object[] { userName }, Integer.class);
				boolean isExist = count > 0 ? true : false;
				return isExist;
			} catch (Exception e) {
				logger.error("Exception in isBhostNameExist: ", e);
			}
			return false;
		}

		@Override
		public boolean isBhostExist(String bhId) {
			try {
				String sql = "SELECT count(bhId) FROM bhost WHERE bhId=?";
				int count = jdbcTemplate.queryForObject(sql, new Object[] {bhId }, Integer.class);
				boolean isExist = count > 0 ? true : false;
				return isExist;
			} catch (Exception e) {
				logger.error("Exception in isBhostExist: ", e);
			}
			return false;
		}

		@Override
		public Response deleteBhost(String bhId) throws Exception {
			Response response = CommonUtils.getResponseObject("Delete host data");
			try {
				String sql = "delete from bhost WHERE bhId=?";
				int rows = jdbcTemplate.update(sql, bhId);
				if (rows > 1) {
					response.setStatus(StatusCode.SUCCESS.name());
				} else {
					response.setStatus(StatusCode.ERROR.name());
				}
			} catch (Exception e) {
				logger.error("Exception in deleteBhost", e);
				response.setStatus(StatusCode.ERROR.name());
				response.setErrors(e.getMessage());
			}
			return response;
		}

		@Override
		public BusinessHost authenticateBhost(BusinessHost bhost) throws Exception {
			try {
				String sql = "SELECT * FROM bhost where userName=? and password=? and isActive=1";
				return (BusinessHost) jdbcTemplate.queryForObject(sql, new Object[] { bhost.getUserName(), bhost.getPassword() },
						new BeanPropertyRowMapper<BusinessHost>(BusinessHost.class));
			} catch (Exception e) {
				logger.error("Exception in authenticate", e);
			}
			return null;
		}

		
		

		@Override
		public List<BusinessHost> getBhostsByCompany(String companyName) throws Exception {
			try {
				String sql = "SELECT * FROM bhost where companyName=?";
				List<BusinessHost> bhosts = jdbcTemplate.query(sql, new Object[] { companyName },
						new BeanPropertyRowMapper<BusinessHost>(BusinessHost.class));
				return bhosts;
			} catch (Exception e) {
				logger.error("Exception in getBhosts", e);
			}
			return null;
		}

		

		
	
}
