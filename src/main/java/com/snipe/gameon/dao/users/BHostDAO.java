package com.snipe.gameon.dao.users;

import java.util.List;

import com.snipe.gameon.domain.users.BusinessHost;
import com.snipe.gameon.response.Response;


public interface BHostDAO {

	public Response saveBhost(BusinessHost bhost) throws Exception;

	public Response updateBhost(BusinessHost bhost) throws Exception;

	public Response deleteBhost(String bhId) throws Exception;

	public BusinessHost getBhost(String bhId) throws Exception;

	public List<BusinessHost> getBhosts() throws Exception;

	public BusinessHost authenticateBhost(BusinessHost bhost) throws Exception;

	public boolean isBhostNameExist(String userName) throws Exception;

	public boolean isBhostExist(String bhId) throws Exception;

	

	public List<BusinessHost> getBhostsByCompany(String organizationId) throws Exception;


}
