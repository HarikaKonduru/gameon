package com.snipe.gameon.dao.users;

import java.util.List;

import com.snipe.gameon.domain.users.Participant;
import com.snipe.gameon.response.Response;

public interface ParticipantDAO {

	public Response saveParticipant(Participant participant) throws Exception;

	public Response updateParticipant(Participant participant) throws Exception;

	public Response deleteParticipant(String participantId) throws Exception;

	public Participant getParticipant(String participantId) throws Exception;

	public List<Participant> getParticipants() throws Exception;

	public boolean isParticipantNameExist(String userName) throws Exception;

	public boolean isParticipantExist(String participantId) throws Exception;

	public Participant authenticateP(Participant participant) throws Exception;



}
